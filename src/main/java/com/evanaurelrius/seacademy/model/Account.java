package com.evanaurelrius.seacademy.model;

import javax.persistence.*;

@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", updatable = false)
    private long id;

    @Column(name = "first_name") 
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "sid",unique=true)
    private String sid;

    @Column(name = "password")
    private String password;

    public Account() {
    }

    public Account(String firstName, String lastName, String sid, String password) { 
        this.firstName = firstName;
        this.lastName = lastName;
        this.sid = sid;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
