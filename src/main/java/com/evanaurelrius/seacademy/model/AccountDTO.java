package com.evanaurelrius.seacademy.model;

import lombok.Data;

@Data
public class AccountDTO {
    private String firstName;
    private String lastName;
    private String sid;
    private String password;

    public AccountDTO() {}

    public AccountDTO(String firstName, String lastName, String sid, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sid = sid;
        this.password = password;
    }
}
