package com.evanaurelrius.seacademy.controller;

import com.evanaurelrius.seacademy.model.Account;
import com.evanaurelrius.seacademy.model.BalanceDTO;
import com.evanaurelrius.seacademy.service.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
public class BalanceController {

    private String error="";

    private static final String BAL_ATTR = "balance";
    private static final String FULLNAME = "fullName";
    private static final String TO_LOGIN = "redirect:/login";

    @Autowired
    BalanceService balanceService;

    @GetMapping("/balanceBox")
    public String balanceBox(Model model, HttpSession session) {
        String fullName = (String) session.getAttribute(FULLNAME);
        if(fullName!=null){
            BalanceDTO balanceDto = new BalanceDTO();
            model.addAttribute("name", fullName);
            model.addAttribute("balanceDto", balanceDto);
            model.addAttribute(BAL_ATTR,balanceService.getStringBalance());
            if(Boolean.FALSE.equals(error.isEmpty())) {
                model.addAttribute("error", error);
                error = "";
            }
        } else {
            return TO_LOGIN;
        }
        return BAL_ATTR;
    }

    @PostMapping("/topup")
    public String topup(@ModelAttribute BalanceDTO balanceDto, Model model, HttpSession session) {
        String fullName = (String) session.getAttribute(FULLNAME);
        if(fullName==null) return TO_LOGIN;

        String balance = balanceService.getStringBalance();
        String newBalance = balanceService.addBalance(balanceDto.getAmount());
        if(newBalance.equals(balance)) {
            error = "Invalid amount to top up!";
        } else{
            model.addAttribute(BAL_ATTR,newBalance);
        }        return "redirect:/balanceBox";
    }

    @PostMapping("/withdraw")
    public String withdraw(@ModelAttribute BalanceDTO balanceDto, Model model, HttpSession session) {
        String fullName = (String) session.getAttribute(FULLNAME);
        if(fullName==null) return TO_LOGIN;

        String balance = balanceService.getStringBalance();
        String newBalance = balanceService.subtractBalance(balanceDto.getAmount());
        if(newBalance.equals(balance)) {
            error = "Invalid amount to withdraw!";
        } else{
            model.addAttribute(BAL_ATTR,newBalance);
        }
        return "redirect:/balanceBox";
    }
}
