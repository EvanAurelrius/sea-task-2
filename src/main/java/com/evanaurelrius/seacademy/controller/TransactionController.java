package com.evanaurelrius.seacademy.controller;

import com.evanaurelrius.seacademy.model.Product;
import com.evanaurelrius.seacademy.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TransactionController {

    @Autowired
    ProductService productService;

    private static final String TO_LOGIN = "redirect:/login";


    @GetMapping(path = "/transaction")
    public String balanceBox(Model model, HttpSession session) {
        String fullName = (String) session.getAttribute("fullName");
        if(fullName!=null){
            model.addAttribute("name", fullName);
        } else {
            return TO_LOGIN;
        }
        String uid = (String) session.getAttribute("uid");
        List<Product> products = productService.getAllPurchasedProductByTimeDesc(Long.parseLong(uid));
        model.addAttribute("products", products);
        model.addAttribute("sortingMechanism", "Latest first");

        return "transaction";
    }

    @GetMapping(path = "/sortTransaction/{sortingMechanism}")
    public String productSort(@PathVariable("sortingMechanism") String sortBy, Model model, HttpSession session) {
        String fullName = (String) session.getAttribute("fullName");
        if(fullName==null){
            return TO_LOGIN;
        }
        Long uid = Long.parseLong((String)session.getAttribute("uid"));
        List<Product> products = new ArrayList<>();
        String mechanism = "";
        switch (sortBy) {
            case "nameAsc":
                products = productService.getAllPurchasedProductByNameAsc(uid);
                mechanism = "A to Z";
                break;
            case "nameDesc":
                products = productService.getAllPurchasedProductByNameDesc(uid);
                mechanism = "Z to A";
                break;
            case "timeAsc":
                products = productService.getAllPurchasedProductByTimeAsc(uid);
                mechanism = "Oldest first";
                break;
            case "timeDesc":
                products = productService.getAllPurchasedProductByTimeDesc(uid);
                mechanism = "Latest first";
                break;
            default:
                break;
        }
        model.addAttribute("name", fullName);
        model.addAttribute("sortingMechanism", mechanism);
        model.addAttribute("products", products);
        return "transaction";
    }

}
