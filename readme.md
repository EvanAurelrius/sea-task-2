# Honesty Canteen
A honesty canteen of SD SEA Sentosa

## About Honesty Canteen
This web application digitalize honesty canteen.
Where students can sell and buy anything.
Every stundents are expected to be honest when using this honesty canteen.
Therefore, this web has authentication and authorization where only stundents with a valid stundent's id can buy/sell anything and add/withdraw money from balance box.

## Meet The Developer!
Evan Aurelrius - 2006595904 - Ilmu Komputer UI 2020

## All Phase/Feature
- Everyone can the items for sale
- Only students can buy/sell items
- Only students can see the balance box amount
- Only students call add/withdraw balance from balance box
- Only students with a valid student's id can register an account
- Only students with a valid student's id and password can log in
- Everyone logged in student has their own transaction history

### Valid student's id:
- ID that consist of a 5 digits number from 0-9. The two last digits are the sum of the three first digits.

## Web Hosting Server
 - Heroku
  
## Tech Stack
 - Java Spring Boot w/ Gradle
 - Postgre SQL Database
 - Bootstrap 5

## Profiling Tool
- JProfiler
- Intellij idea

## Code Quality Inspection Tool
- Sonarqube

## Sonarqube Result
![alt text](https://gitlab.com/EvanAurelrius/sea-task-2/-/raw/master/sonarqube.jpg)